#include "controllerclass.h"

ControllerClass::ControllerClass(QObject *parent, WorkerClass *workerClass) :
    QObject(parent), d_workerClass(workerClass)
{

    workerClass->moveToThread(&d_workerThread);

    this->connect(&d_workerThread, &QThread::started, workerClass, &WorkerClass::doWork);

    this->connect(&d_workerThread, &QThread::finished, this, &ControllerClass::workerClassFinished);

    d_workerThread.start();
}

ControllerClass::~ControllerClass()
{

    d_workerThread.quit();

    d_workerThread.wait();

}

void ControllerClass::startWorkerClass()
{

    d_workerThread.start();

}
