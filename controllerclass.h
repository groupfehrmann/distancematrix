#ifndef CONTROLLERCLASS_H
#define CONTROLLERCLASS_H

#include <QObject>
#include <QThread>

#include "workerclass.h"

class ControllerClass : public QObject
{

    Q_OBJECT

public:

    explicit ControllerClass(QObject *parent = nullptr, WorkerClass *workerClass = nullptr);

    ~ControllerClass();

private:

    WorkerClass *d_workerClass;

    QThread d_workerThread;

signals:

    void workerClassFinished();

public slots:

    void startWorkerClass();

};

#endif // CONTROLLERCLASS_H
