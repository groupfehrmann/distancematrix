#include "logging.h"

LoggingSortFilterProxyModel::LoggingSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

bool LoggingSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    LoggingModel *loggingModel = static_cast<LoggingModel *>(this->sourceModel());

    return loggingModel->show(sourceRow);

}

LoggingModel::LoggingModel(QObject *parent) :
    QAbstractListModel(parent), d_showMessage(true), d_showWarning(true), d_showError(true)
{

}

void LoggingModel::appendLogItem(LoggingModel::Type type, const QString &text)
{

    this->beginInsertRows(QModelIndex(), d_logItems.size(), d_logItems.size());

    d_logItems.append({QDateTime::currentDateTime(), type, text});

    this->endInsertRows();

}

void LoggingModel::clear()
{

    this->beginResetModel();

    d_logItems.clear();

    this->endResetModel();

}

QVariant LoggingModel::data(QModelIndex const &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    LoggingModel::LogItem const &logItem(d_logItems.at(index.row()));

    if (role == Qt::DisplayRole) {

        switch (logItem.type) {

        case LoggingModel::MESSAGE :
            return logItem.dataTimeStamp.toString("d MMM yyyy - hh:mm:ss") + "\t" + logItem.text;

        case LoggingModel::WARNING :
            return logItem.dataTimeStamp.toString("d MMM yyyy - hh:mm:ss") + "\tWARNING\t" + logItem.text;

        case LoggingModel::ERROR :
            return logItem.dataTimeStamp.toString("d MMM yyyy - hh:mm:ss") + "\tERROR\t" + logItem.text;

        }

    }

    return QVariant();

}

int LoggingModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;

    return d_logItems.count();

}

void LoggingModel::setShowError(bool show)
{

    this->beginResetModel();

    d_showError = show;

    this->endResetModel();

}

void LoggingModel::setShowWarning(bool show)
{

    this->beginResetModel();

    d_showWarning = show;

    this->endResetModel();

}

void LoggingModel::setShowMessage(bool show)
{
    this->beginResetModel();

    d_showMessage = show;

    this->endResetModel();

}

bool LoggingModel::show(int index) const
{

    switch (d_logItems.at(index).type) {

        case MESSAGE : return d_showMessage;

        case WARNING : return d_showWarning;

        case ERROR : return d_showError;

    }

}
