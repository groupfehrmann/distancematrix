#ifndef LOGGING_H
#define LOGGING_H

#include <QObject>
#include <QDateTime>
#include <QList>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>


class LoggingSortFilterProxyModel : public QSortFilterProxyModel
{

public:

    LoggingSortFilterProxyModel(QObject *parent = nullptr);

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

};

class LoggingModel : public QAbstractListModel
{

    Q_OBJECT

public:

    enum Type {

        MESSAGE,

        WARNING,

        ERROR,

    };

    struct LogItem {

        QDateTime dataTimeStamp;

        Type type;

        QString text;

    };

    LoggingModel(QObject *parent = nullptr);

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    bool show(int index) const;

private:

    QList<LogItem> d_logItems;

    bool d_showMessage;

    bool d_showWarning;

    bool d_showError;

    void setStandardMessage();

public slots:

    void appendLogItem(LoggingModel::Type type, const QString &text);

    void clear();

    void setShowError(bool show);

    void setShowWarning(bool show);

    void setShowMessage(bool show);

};

#endif // LOGGING_H
