#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QTextStream>

#include "logging.h"
#include "workerclass.h"
#include "controllerclass.h"
#include "progressclass.h"
#include "rtclass.h"

int main(int argc, char *argv[])
{

    QCoreApplication application(argc, argv);

    qRegisterMetaType<LoggingModel::Type>("LoggingModel::Type");

    QCoreApplication::setApplicationName("Create distance matrix");

    QCoreApplication::setApplicationVersion("Last updated 9th of April 2020");


    QCommandLineParser commandLineParser;

    commandLineParser.setApplicationDescription("This application creates a distance matrix with specified distance function");

    commandLineParser.addHelpOption();

    commandLineParser.addVersionOption();


    QCommandLineOption sourceOption(QStringList() << "s" << "source", "Source file containing matrix with column and row identifiers", "source");

    commandLineParser.addOption(sourceOption);


    QCommandLineOption orientationOption(QStringList() << "o" << "orientation", "Orientation used to determine distance matrix [row, column]", "orientation");

    commandLineParser.addOption(orientationOption);


    QCommandLineOption distanceFunctionOption(QStringList() << "d" << "distance", "Function used to created distances [dCor, oneMinusdCor, dCov, oneMinusdCov]", "distance function");

    commandLineParser.addOption(distanceFunctionOption);


    QCommandLineOption outputDirectoryOption(QStringList() << "od" << "outputDirectory", "Directory used for output", "output directory");

    commandLineParser.addOption(outputDirectoryOption);


    QCommandLineOption outputFilenameOption(QStringList() << "of" << "outputFilename", "Filename used for output", "output filename");

    commandLineParser.addOption(outputFilenameOption);


    QCommandLineOption logFilenameOption(QStringList() << "lf" << "logFilename", "Filename used for log", "log filename");

    commandLineParser.addOption(logFilenameOption);


    QCommandLineOption threadCountOption(QStringList() << "tc" << "threadCount", "Maximum number of threads used for computation (default = " + QString::number(QThread::idealThreadCount()) + ")", "output directory");

    commandLineParser.addOption(threadCountOption);


    QCommandLineOption rFileFormat(QStringList() << "rf" << "rFileFormat", "Option to accomodate R tab delimited export format: true -> first token in header defines column with first data -- false -> first token in header defines column with row identifiers", "f file format");

    commandLineParser.addOption(rFileFormat);


    commandLineParser.process(application);

    LoggingModel loggingModel;

    LoggingSortFilterProxyModel loggingSortFilterProxyModel;

    loggingSortFilterProxyModel.setSourceModel(&loggingModel);


    ProgressClass progressClass;

    WorkerClass workerClass;


    if (commandLineParser.isSet(threadCountOption))
        QThreadPool::globalInstance()->setMaxThreadCount(commandLineParser.value(threadCountOption).toInt());

    loggingModel.appendLogItem(LoggingModel::MESSAGE, "maximum number of theads used for computations : " + QString::number(QThreadPool::globalInstance()->maxThreadCount()));

    workerClass.parameters.source = commandLineParser.value(sourceOption);

    workerClass.parameters.orientation = commandLineParser.value(orientationOption);

    workerClass.parameters.distanceFunction = commandLineParser.value(distanceFunctionOption);

    workerClass.parameters.validDistanceFunctions.insert("dCor");

    workerClass.parameters.validDistanceFunctions.insert("oneMinusdCor");

    workerClass.parameters.validDistanceFunctions.insert("dCov");

    workerClass.parameters.validDistanceFunctions.insert("oneMinusdCov");

    workerClass.parameters.outputDirectory = commandLineParser.value(outputDirectoryOption);

    if (commandLineParser.isSet(outputFilenameOption))
        workerClass.parameters.outputFilename = commandLineParser.value(outputFilenameOption);
    else
        workerClass.parameters.outputFilename = "distanceMatrix.txt";

    if (commandLineParser.value(rFileFormat) == "true")
        workerClass.parameters.RFileFormat = true;
    else
        workerClass.parameters.RFileFormat = false;

    QObject::connect(&workerClass, &WorkerClass::appendLogItem, &loggingModel, &LoggingModel::appendLogItem);

    QObject::connect(&workerClass, &WorkerClass::startProcess, &progressClass, &ProgressClass::startProcess);

    QObject::connect(&workerClass, &WorkerClass::stopProcess, &progressClass, &ProgressClass::stopProcess);

    QObject::connect(&workerClass, &WorkerClass::startProgress, &progressClass, &ProgressClass::startProgress);

    QObject::connect(&workerClass, &WorkerClass::stopProgress, &progressClass, &ProgressClass::stopProgress);

    QObject::connect(&workerClass, &WorkerClass::updateProgress, &progressClass, &ProgressClass::updateProgress);


    RTClass rtClass(nullptr, &loggingSortFilterProxyModel, &progressClass);


    ControllerClass controllerClass(nullptr, &workerClass);

    QObject::connect(&workerClass, &WorkerClass::updateProgress, &progressClass, &ProgressClass::updateProgress);

    QObject::connect(&controllerClass, &ControllerClass::workerClassFinished, &application, &QCoreApplication::quit);

    controllerClass.startWorkerClass();


    int code = application.exec();

    loggingModel.appendLogItem(LoggingModel::MESSAGE, "total time spend on process :" + ProgressClass::secondsToTimeString(progressClass.timeElapsedInSeconds()));


    QString logPath;

    if (commandLineParser.isSet(logFilenameOption))
        logPath = workerClass.parameters.outputDirectory + "/" + commandLineParser.value(logFilenameOption);
    else
        logPath = workerClass.parameters.outputDirectory + "/logFile.txt";

    QFile logFile(logPath);

    if (!logFile.open(QIODevice::WriteOnly | QIODevice::Text)) {

        static_cast<LoggingModel>(loggingSortFilterProxyModel.sourceModel()).appendLogItem(LoggingModel::ERROR, "could not open file \"" + workerClass.parameters.outputDirectory + "/logFile.txt\"");

        rtClass.renderTerminal();

        QTextStream out(stdout);

        out <<  QString("\033[0m \033[1;34\033[0m") << endl;

        return code;

    }

    progressClass.startProgress(0, "Exporting", 0, loggingSortFilterProxyModel.rowCount());

    QTextStream outLogFile(&logFile);

    for (int i = 0; i < loggingSortFilterProxyModel.rowCount(); ++i) {

        outLogFile << loggingSortFilterProxyModel.data(loggingSortFilterProxyModel.index(i, 0)).toString() << endl;

        progressClass.updateProgress(0, i + 1);

    }

    progressClass.stopProgress(0);


    rtClass.renderTerminal();

    QTextStream out(stdout);

    out <<  QString("\033[0m \033[1;34\033[0m") << endl;

    return code;

}
