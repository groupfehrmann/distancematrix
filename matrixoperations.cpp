#include "matrixoperations.h"

QVector<double> MatrixOperations::columnVector(const QVector<QVector<double> > &matrix, int columnIndex)
{

    QVector<double> _columnVector;

    _columnVector.reserve(matrix.size());

    for (const QVector<double> &vector : matrix)
        _columnVector << vector.at(columnIndex);

    return _columnVector;

}

QVector<double> MatrixOperations::rowVector_x_matrix(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    QVector<double> result(matrix.first().size(), 0.0);

    for (int i = 0; i < rowVector.size(); ++i) {

        const double &temp(rowVector.at(i));

        const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

        for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
            result[j] += currentRowVectorOfMatrix.at(j) * temp;

    }

    return result;

}

QVector<double> MatrixOperations::rowVector_x_matrix_multiThreaded(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    auto mapFunctor = [&](const int &index) {

        return VectorOperations::vsMul(matrix.at(index), rowVector.at(index));

    };

    auto reduceFunctor = [](QVector<double> &result, const QVector<double> &intermediateResult) {

        VectorOperations::vsMul_vsMul_vvAdd_inplace(result, 1.0, intermediateResult, 1.0);

    };

   return QtConcurrent::blockingMappedReduced<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(rowVector.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor), std::function<void(QVector<double> &, const QVector<double> &)>(reduceFunctor));

}

QVector<double> &MatrixOperations::rowVector_x_matrix_inplace(QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    rowVector = MatrixOperations::rowVector_x_matrix(rowVector, matrix);

    return rowVector;

}

QVector<double> &MatrixOperations::rowVector_x_matrix_inplace_multiThreaded(QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    rowVector = MatrixOperations::rowVector_x_matrix_multiThreaded(rowVector, matrix);

    return rowVector;

}

QVector<double> MatrixOperations::matrix_x_columnVector(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
{

    QVector<double> result;

    result.reserve(matrix.size());

    for (const QVector<double> &currentRowVector : matrix)
        result << static_cast<double>(VectorOperations::dotProduct(currentRowVector, columnVector));

    return result;

}


QVector<double> MatrixOperations::matrix_x_columnVector_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
{

    return QtConcurrent::blockingMapped<QVector<double> >(matrix, std::bind(VectorOperations::dotProduct, std::placeholders::_1, std::cref(columnVector)));

}

QVector<double> &MatrixOperations::matrix_x_columnVector_inplace(const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
{

    columnVector = MatrixOperations::matrix_x_columnVector(matrix, columnVector);

    return columnVector;

}

QVector<double> &MatrixOperations::matrix_x_columnVector_inplace_multiThreaded(const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
{

    columnVector = MatrixOperations::matrix_x_columnVector_multiThreaded(matrix, columnVector);

    return columnVector;

}

QVector<QVector<double> > MatrixOperations::matrix1_x_matrix2(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    QVector<QVector<double> > result;

    result.reserve(matrix1.size());

    for (const QVector<double> &vector : matrix1)
        result << MatrixOperations::rowVector_x_matrix(vector, matrix2);

    return result;

}

QVector<QVector<double> > MatrixOperations::matrix1_x_matrix2_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    auto mapFunctor = [&](const QVector<double> &vector) {

        return MatrixOperations::rowVector_x_matrix(vector, matrix2);

    };

    return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix1, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

}

QVector<QVector<double> > &MatrixOperations::matrix1_x_matrix2_inplace(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    for (QVector<double> &currentRowVectorOfMatrix1 : matrix1)
        MatrixOperations::rowVector_x_matrix_inplace(currentRowVectorOfMatrix1, matrix2);

    return matrix1;

}


QVector<QVector<double> > &MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    QtConcurrent::blockingMap(matrix1, std::bind(MatrixOperations::rowVector_x_matrix_inplace, std::placeholders::_1, std::cref(matrix2)));

    return matrix1;

}

QVector<QVector<double> > MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose(const QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    QVector<QVector<double> > result(matrix.size(), QVector<double>(matrix.size()));

    for (int i = 0; i < matrix.size(); ++i) {

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(i), 1.0, scalars, 1.0);

        QVector<double> &currentRowVectorInResult(result[i]);

        currentRowVectorInResult[i] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(i)));

        for (int j = i + 1; j < matrix.size(); ++j)
            result[j][i] = currentRowVectorInResult[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

    }

    return result;

}

QVector<QVector<double> > MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    auto mapFunctor = [&](const int &index) {

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(index), 1.0, scalars, 1.0);

        QVector<double> result(matrix.size());

        result[index] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(index)));

        for (int j = index + 1; j < matrix.size(); ++j)
            result[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

        return result;

    };

    QVector<QVector<double> > result = QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor));

    auto mirrorFunctor = [&](int &index) {

        QVector<double> &currentRowInResult(result[index]);

        for (int i = 0; i < index; ++i)
            currentRowInResult[i] = result.at(i).at(index);

    };

    QVector<int> sequenceOfIndexes = FunctionsForMultiThreading::createSequenceOfIndexes(result.size(), 0, 1);

    QtConcurrent::blockingMap(sequenceOfIndexes, mirrorFunctor);

    return result;

}

QVector<QVector<double> > &MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace(QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    for (int i = 0; i < matrix.size(); ++i) {

        QVector<double> intermediateVector(matrix.size());

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(i), 1.0, scalars, 1.0);

        for (int j = 0; j < i; ++j)
            intermediateVector[j] = matrix.at(j).at(i);

        intermediateVector[i] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(i)));

        for (int j = i + 1; j < matrix.size(); ++j)
            intermediateVector[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

        matrix[i] = intermediateVector;

    }

    return matrix;

}

QVector<QVector<double> > &MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace_multiThreaded(QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    FunctionsForMultiThreading::detachMatrix(matrix);

    QSemaphore semaphore(1);

    QReadWriteLock readWriteLock;

    auto functor = [&](const double &value1, const double &value2){ return value1 * value2;};

    auto processor = [&](const QVector<int> &indexesToProcess) {

        for (const int &indexToProcess : indexesToProcess) {

            semaphore.acquire(indexToProcess + 1);

            readWriteLock.lockForRead();

            QVector<double> intermediateVector(matrix.size());

            QVector<double> currentRowVectorInMatrix = VectorOperations::applyFunctor_elementWise(matrix.at(indexToProcess), scalars, functor);

            intermediateVector[indexToProcess] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(indexToProcess)));

            if (indexToProcess + 1 < matrix.size()) {

                intermediateVector[indexToProcess + 1] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(indexToProcess + 1)));

                semaphore.release(indexToProcess + 2);

            }

            for (int i = indexToProcess + 2; i < matrix.size(); ++i)
                intermediateVector[i] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(i)));

            readWriteLock.unlock();

            readWriteLock.lockForWrite();

            matrix[indexToProcess] = intermediateVector;

            readWriteLock.unlock();

        }

    };


    QFutureSynchronizer<void> futureSynchronizer;

    QVector<QVector<int> > schedule = FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount());

    for (const QVector<int> &indexesToProcess : schedule)
        futureSynchronizer.addFuture(QtConcurrent::run(processor, indexesToProcess));

    futureSynchronizer.waitForFinished();

    futureSynchronizer.clearFutures();

    auto mirrorFunction = [&](const QVector<int> &indexesToProcess) {

        for (const int &indexToProcess : indexesToProcess) {

            QVector<double> &currentRowVector(matrix[indexToProcess]);

            for (int i = 0; i < indexToProcess; ++i)
                currentRowVector[i] = matrix.at(i).at(indexToProcess);

        }

    };

    for (const QVector<int> &indexesToProcess : schedule)
        futureSynchronizer.addFuture(QtConcurrent::run(mirrorFunction, indexesToProcess));

    futureSynchronizer.waitForFinished();

    return matrix;

}

QVector<QVector<double> > MatrixOperations::subMatrixBasedOnColumnIndexes(const QVector<QVector<double> > &matrix, QList<int> columnIndexes)
{

    QVector<QVector<double> > subMatrix;

    subMatrix.reserve(matrix.size());

    for (const QVector<double> &vector : matrix) {

        QVector<double> temp;

        temp.reserve(columnIndexes.size());

        for (const int &columnIndex : columnIndexes)
            temp << vector.at(columnIndex);

        subMatrix << temp;
    }

    return subMatrix;

}
