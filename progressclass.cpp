#include "progressclass.h"

QString ProgressClass::secondsToTimeString(qint64 s)
{

    qint64 days = s / 86400;

    qint64 seconds = s - days * 86400;

    qint64 hours = seconds / 3600;

    seconds -= hours * 3600;

    qint64 minutes = seconds / 60;

    seconds -= minutes * 60;

    QString str;

    if (days)
        str += QString("%1d ").arg(days, 1);

    if (hours >= 1)
        str += QString("%1h ").arg(hours, 2);

    if (minutes >= 1)
        str += QString("%1m ").arg(minutes, 2);

    if (seconds >= 1)
        str += QString("%1s ").arg(seconds, 2);

    if (str.isEmpty())
        str = QString();

    return str;

}

ProgressClass::ProgressClass(QObject *parent) : QObject(parent)
{

}

void ProgressClass::startProcess()
{

    d_dateTimeStampOfStartOfProcess = QDateTime::currentDateTime();

}

void ProgressClass::stopProcess()
{

}

void ProgressClass::startProgress(int key, const QString &label, const double &minimumValue, const double &maximumValue)
{

    ProgressItem &progressItem(d_progressItems[key]);

    progressItem.label = label;

    progressItem.minimumValue = minimumValue;

    progressItem.maximumValue = maximumValue;

    progressItem.currentProgressValue = 0.0;

    progressItem.etaInSeconds = 0;

    progressItem.dateTimeStampOfStart = QDateTime::currentDateTime();

    progressItem.dateTimeStampOfLastUpdate = QDateTime::currentDateTime();

}

void ProgressClass::stopProgress(int key)
{

    d_progressItems.remove(key);

}

void ProgressClass::updateProgress(int key, const double &value)
{

    ProgressItem &progressItem(d_progressItems[key]);

    if ((value - progressItem.minimumValue) < std::numeric_limits<double>::min())
        return;

    double currentProgressValue = (value - progressItem.minimumValue) / (progressItem.maximumValue - progressItem.minimumValue);

    if ((currentProgressValue - progressItem.currentProgressValue) < std::numeric_limits<double>::min())
        return;

    progressItem.currentProgressValue = currentProgressValue;

    progressItem.dateTimeStampOfLastUpdate.secsTo(QDateTime::currentDateTime());

    progressItem.etaInSeconds = static_cast<qint64>(static_cast<double>(progressItem.dateTimeStampOfStart.secsTo(QDateTime::currentDateTime())) * (1.0 - progressItem.currentProgressValue) / progressItem.currentProgressValue) - progressItem.dateTimeStampOfLastUpdate.secsTo(QDateTime::currentDateTime());

    progressItem.dateTimeStampOfLastUpdate = QDateTime::currentDateTime();

}

qint64 ProgressClass::timeElapsedInSeconds() const
{

    return d_dateTimeStampOfStartOfProcess.secsTo(QDateTime::currentDateTime());

}

int ProgressClass::processCount() const
{

    return d_progressItems.count();

}

const QMap<int, ProgressClass::ProgressItem> &ProgressClass::progressItems() const
{

    return d_progressItems;

}
