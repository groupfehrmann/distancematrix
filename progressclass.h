#ifndef PROGRESSCLASS_H
#define PROGRESSCLASS_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QMap>

class ProgressClass : public QObject
{

    Q_OBJECT

public:

    struct ProgressItem {

        double minimumValue;

        double maximumValue;

        double currentProgressValue;

        qint64 etaInSeconds;

        QDateTime dateTimeStampOfLastUpdate;

        QDateTime dateTimeStampOfStart;

        QString label;

    };

    static QString secondsToTimeString(qint64 s);

    explicit ProgressClass(QObject *parent = nullptr);

    qint64 timeElapsedInSeconds() const;

    int processCount() const;

    const QMap<int, ProgressItem> &progressItems() const;

private:

    QDateTime d_dateTimeStampOfStartOfProcess;

    QMap<int, ProgressItem> d_progressItems;

signals:

public slots:

    void startProcess();

    void stopProcess();

    void startProgress(int index, const QString &label, const double &minimumValue, const double &maximumValue);

    void stopProgress(int index);

    void updateProgress(int index, const double &value);

};

#endif // PROGRESSCLASS_H
