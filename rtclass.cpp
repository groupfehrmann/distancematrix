#include "rtclass.h"

#include "rxterm/terminal.hpp"
#include "rxterm/style.hpp"
#include "rxterm/image.hpp"
#include "rxterm/reflow.hpp"
#include "rxterm/components/text.hpp"
#include "rxterm/components/stacklayout.hpp"
#include "rxterm/components/flowlayout.hpp"
#include "rxterm/components/progress.hpp"
#include "rxterm/components/maxwidth.hpp"


RTClass::RTClass(QObject *parent, LoggingSortFilterProxyModel *loggingSortFilterProxyModel, ProgressClass *progressClass) :
    QObject(parent), d_loggingSortFilterProxyModel(loggingSortFilterProxyModel), d_progressClass(progressClass)
{

    d_vt = new rxterm::VirtualTerminal;

    d_timer.setInterval(500);

    this->connect(&d_timerThread, SIGNAL(started()), &d_timer, SLOT(start()));

    this->connect(&d_timerThread, SIGNAL(finished()), &d_timer, SLOT(stop()));

    d_timer.moveToThread(&d_timerThread);

    this->connect(&d_timer, &QTimer::timeout, this, [&](){ this->renderTerminal();}, Qt::DirectConnection);

    d_timerThread.start();

}

RTClass::~RTClass()
{
    d_timerThread.quit();

    d_timerThread.wait();

    delete  d_vt;

}

void RTClass::renderTerminal()
{

    rxterm::StackLayout<> mainStackLayout;

    mainStackLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black, rxterm::Font::Bold}, QCoreApplication::applicationName().toStdString() + " - " + QCoreApplication::applicationVersion().toStdString()));

    rxterm::StackLayout<> progressLayout;

    progressLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black, rxterm::Font::Bold}, "\nProgress\n"));

    for (const auto &element : d_progressClass->progressItems()) {

        rxterm::FlowLayout<> progressItemLayout;

        std::string label1 = element.label.toStdString() + " -> ";

        label1.insert(label1.begin(), 40 - label1.length(), ' ');

        progressItemLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black}, label1));

        std::string label2 =  "ETA : " + ProgressClass::secondsToTimeString(element.etaInSeconds - element.dateTimeStampOfLastUpdate.secsTo(QDateTime::currentDateTime())).toStdString();

        label2.append(25 - label2.length(), ' ');

        progressItemLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black}, label2));

        progressItemLayout.children.push_back(rxterm::MaxWidth(40, rxterm::Progress(static_cast<float>(element.currentProgressValue))));

        progressLayout.children.push_back(progressItemLayout);

    }

    mainStackLayout.children.push_back(progressLayout);

    mainStackLayout.children.push_back(rxterm::FlowLayout<>{rxterm::Text({rxterm::FontColor::Black, rxterm::Font::Bold}, "\nTime elapsed\t: "), rxterm::Text({rxterm::FontColor::Black}, "\n" + ProgressClass::secondsToTimeString(d_progressClass->timeElapsedInSeconds()).toStdString())});

    mainStackLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black, rxterm::Font::Bold}, "\nLog"));

    mainStackLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black}, "Showing last log items out of " + QString::number(d_loggingSortFilterProxyModel->sourceModel()->rowCount()).toStdString()));

    int rowCount = d_loggingSortFilterProxyModel->rowCount();

    int startRow = rowCount - 10;

    if (startRow < 0)
        startRow = 0;

    for (int i = startRow; i < rowCount; ++i)
        mainStackLayout.children.push_back(rxterm::Text({rxterm::FontColor::Black}, d_loggingSortFilterProxyModel->data(d_loggingSortFilterProxyModel->index(i, 0)).toString().toStdString()));

    auto renderToTerm = [](auto const& d_vt, unsigned const w, rxterm::Component const& c) {

        return d_vt.flip(c.render(w).toString());

    };

    *d_vt = renderToTerm(*d_vt, 200, mainStackLayout);

}
