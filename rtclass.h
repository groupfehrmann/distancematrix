#ifndef RTCLASS_H
#define RTCLASS_H

#include <QObject>
#include <QCoreApplication>
#include <QTimer>
#include <QThread>
#include <QDateTime>

#include "logging.h"
#include "progressclass.h"

namespace rxterm {

    struct VirtualTerminal;

}

class RTClass : public QObject
{

    Q_OBJECT

public:

    explicit RTClass(QObject *parent = nullptr, LoggingSortFilterProxyModel *loggingSortFilterProxyModel = nullptr, ProgressClass *progressClass = nullptr);

    ~RTClass();

private:

    LoggingSortFilterProxyModel *d_loggingSortFilterProxyModel;

    ProgressClass *d_progressClass;

    rxterm::VirtualTerminal *d_vt;

    QTimer d_timer;

    QThread d_timerThread;

signals:

public slots:

    void renderTerminal();

};

#endif // RTCLASS_H
