#include "workerclass.h"

WorkerClass::WorkerClass(QObject *parent) :
    QObject(parent)
{

}

void WorkerClass::doWork()
{

    if (!this->areParemetersValid()) {

        QThread::currentThread()->quit();

        return;
    }

    emit startProcess();

    QFile sourceFile(parameters.source);

    if (!sourceFile.exists() || !sourceFile.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LoggingModel::ERROR, "could not open file \"" + parameters.source + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Reading source file", 0, sourceFile.size());

    QStringList columnIdentifiers = QString(sourceFile.readLine()).remove(QRegExp("[\r\n]")).split("\t", QString::KeepEmptyParts);

    QStringList rowIdentifiers;

    QVector<QVector<double> > matrix;

    if (parameters.RFileFormat == false)
        columnIdentifiers.removeFirst();

    int numberOfLinesRead = 1;

    while (!sourceFile.atEnd()) {

        QStringList tokens = QString(sourceFile.readLine()).remove(QRegExp("[\r\n]")).split("\t", QString::KeepEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() != columnIdentifiers.size() + 1) {

            emit appendLogItem(LoggingModel::ERROR, "the number of tokens read at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(columnIdentifiers.size() + 1) + " tokens detected in the header line");

            QThread::currentThread()->quit();

            return;

        }

        rowIdentifiers << tokens.at(0);

        QVector<double> vector;

        vector.reserve(columnIdentifiers.size());

        bool conversionOk;

        for (int i = 1; i < tokens.size(); ++i) {

            double value = tokens.at(i).toDouble(&conversionOk);

            vector << value;

            if (!conversionOk || std::isnan(value) || std::isinf(value)) {

                emit appendLogItem(LoggingModel::ERROR, "could not convert token \"" + tokens.at(i) +"\" at line " + QString::number(numberOfLinesRead) + " to double");

                QThread::currentThread()->quit();

                return;

            }

        }

        matrix << vector;

        emit updateProgress(0, sourceFile.pos());

    }

    emit stopProgress(0);

    emit appendLogItem(LoggingModel::MESSAGE, "numbers of rows read : " + QString::number(matrix.size()));

    emit appendLogItem(LoggingModel::MESSAGE, "numbers of columns read : " + QString::number(matrix.isEmpty() ? 0 : matrix.first().size()));

    if (parameters.orientation == "column") {

        emit startProgress(0, "Transpose data", 0, 0);

        MatrixOperations::transpose_inplace_multiThreaded(matrix);

        emit stopProgress(0);

    }

    emit startProgress(0, "Calculating distance matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);

    double progressValue = 0.0;

    QTimer timer;

    QThread timerThread;

    timer.setInterval(500);

    this->connect(&timerThread, SIGNAL(started()), &timer, SLOT(start()));

    this->connect(&timerThread, SIGNAL(finished()), &timer, SLOT(stop()));

    timer.moveToThread(&timerThread);

    this->connect(&timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(0, progressValue);}, Qt::DirectConnection);

    timerThread.start();

    if (parameters.distanceFunction == "dCor")
        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, [](const QVector<double> &vector1, const QVector<double> &vector2) {return FastDistanceCorrelation::fastDistanceCorrelation(vector1, vector2, false);}, progressValue);
    else if (parameters.distanceFunction == "oneMinusdCor")
        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, [](const QVector<double> &vector1, const QVector<double> &vector2) {return 1.0 - FastDistanceCorrelation::fastDistanceCorrelation(vector1, vector2, false);}, progressValue);
    else if (parameters.distanceFunction == "dCov")
        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, [](const QVector<double> &vector1, const QVector<double> &vector2) {return std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(vector1, vector2, false, false).first());}, progressValue);
    else if (parameters.distanceFunction == "oneMinusdCov")
        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, [](const QVector<double> &vector1, const QVector<double> &vector2) {return 1.0 - std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(vector1, vector2, false, false).first());}, progressValue);

    timerThread.quit();

    timerThread.wait();

    emit stopProgress(0);

    QFile outputFile(parameters.outputDirectory + "/" + parameters.outputFilename);

    if (!outputFile.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LoggingModel::ERROR, "could not open file \"" + parameters.outputDirectory + "/" + parameters.outputFilename + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Exporting", 0, matrix.size());

    QStringList identifiersForDistanceMatrix = (parameters.orientation == "row") ? rowIdentifiers : columnIdentifiers;

    QTextStream out(&outputFile);

    out << "";

    for (const QString &identifier : identifiersForDistanceMatrix)
        out << "\t" << identifier;

    out << endl;


    for (int i = 0; i < matrix.size(); ++i) {

        out << identifiersForDistanceMatrix.at(i);

        for (const double &value : matrix.at(i))
            out << "\t" << value;

        out << endl;

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit stopProcess();

    QThread::currentThread()->quit();

    return;

}

bool WorkerClass::areParemetersValid()
{

    if (!QFile(parameters.source).exists()) {

        emit appendLogItem(LoggingModel::ERROR, "source \"" + parameters.source + "\" does not exist");

        return false;

    } else
        emit appendLogItem(LoggingModel::MESSAGE, "source : \"" + parameters.source + "\"");

    if (!parameters.validDistanceFunctions.contains(parameters.distanceFunction)) {

        emit appendLogItem(LoggingModel::ERROR, "distance function \"" + parameters.distanceFunction + "\" is not recognized");

        return false;

    } else
        emit appendLogItem(LoggingModel::MESSAGE, "distance function : \"" + parameters.distanceFunction + "\"");


    if (!(parameters.orientation == "row") && !(parameters.orientation == "column")) {

        emit appendLogItem(LoggingModel::ERROR, "orientation \"" + parameters.distanceFunction + "\" is not recognized");

        return false;

    } else
        emit appendLogItem(LoggingModel::MESSAGE, "orientation : " + parameters.orientation);

    if (!QDir(parameters.outputDirectory).exists()) {

        emit appendLogItem(LoggingModel::ERROR, "output directory \"" + parameters.outputDirectory + "\" does not exist");

        return false;

    } else
        emit appendLogItem(LoggingModel::MESSAGE, "output directory : \"" + parameters.outputDirectory + "\"");

    emit appendLogItem(LoggingModel::MESSAGE, "output filename : \"" + parameters.outputFilename + "\"");

    return true;

}
