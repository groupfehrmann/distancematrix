#ifndef WORKERCLASS_H
#define WORKERCLASS_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QSet>
#include <QString>
#include <QThread>

#include "logging.h"
#include "matrixoperations.h"
#include "fastdistancecorrelation.h"

class WorkerClass : public QObject
{

    Q_OBJECT

public:

    struct Parameters {

        QString source;

        QString orientation;

        QString distanceFunction;

        QString outputDirectory;

        QString outputFilename;

        QSet<QString> validDistanceFunctions;

        bool RFileFormat;

    };

    explicit WorkerClass(QObject *parent = nullptr);

    Parameters parameters;

private:

    bool areParemetersValid();

signals:

    void appendLogItem(LoggingModel::Type type, const QString &text);

    void startProcess();

    void stopProcess();

    void startProgress(int index, const QString &label, const double &minimumValue, const double &maximumValue);

    void stopProgress(int index);

    void updateProgress(int index, const double &value);

public slots:

    void doWork();

};

#endif // WORKERCLASS_H
